import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StatisticsPageComponent } from './statistics.component';


@NgModule({
    declarations: [
        StatisticsPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: StatisticsPageComponent
            }
        ])
    ]
})

export class StatisticsPageModule { }
