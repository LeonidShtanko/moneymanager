import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RatePageComponent } from './rate.component';


@NgModule({
    declarations: [
        RatePageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: RatePageComponent
            }
        ])
    ]
})

export class RatePageModule { }
